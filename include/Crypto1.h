#ifndef CRYPTO1_H
#define CRYPTO1_H
#include "Crypto1State.h"
#include <bitset>
using namespace std;


class Crypto1
{
    public:
        Crypto1(Crypto1State state, int t);
        void setInternalState(Crypto1State state);
        Crypto1State getInternalState();
        bool getKeyStream();
        bool nextState(bool input);
        bool prevState(bool input);
        int sumProperty();//return SumProperty of the state
        void eoSumProperty(uint8_t *sp);//return pointer to [oddSp, evenSp]
        uint64_t generateANonce(uint32_t input);


    protected:

    private:
        Crypto1State internalState;
        int time;//how much time it passed
        int tempCount;
        const bitset<16> fa;
        const bitset<16> fb;
        const bitset<32> fc;
};

#endif // CRYPTO1_H
