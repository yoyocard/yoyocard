#ifndef CRYPTO1STATE_H
#define CRYPTO1STATE_H
#include<stdint.h>
#include<string>

using namespace std;
class Crypto1State
{
    public:
        Crypto1State(int h, int t, int unsure=0);
        bool getI(int i);
        void setI(int i, bool b);
        void next(bool input);
        void prev(bool input);
        void printAsString();
    protected:
    private:
        uint16_t head;
        uint32_t tail;
        int FLAG_UNSURE_FIRST_BIT;
};

#endif // CRYPTO1STATE_H
