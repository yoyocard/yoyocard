#include "Crypto1State.h"
#include "stdlib.h"
#include <bitset>
#include <iostream>
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

using namespace std;

Crypto1State::Crypto1State(int h, int t, int unsure):FLAG_UNSURE_FIRST_BIT(unsure), head(h), tail(t)
{

}

bool Crypto1State::getI(int i){
    i = 47 - i;
    if(i<0||i>47) exit(1);
    else if(i<32){
        return CHECK_BIT(tail, i);
    }else return CHECK_BIT(head, i-32);
}

void Crypto1State::setI(int i, bool b){
    i = 47 - i;
    if(i<0||i>47) exit(1);
    else if(i<32)   if(b) tail |= 1<<i;
                    else tail &= ~(1<<i);
        else if(b) head != 1<<(i-32);
            else head &= ~(1<<(i-32));
}

void Crypto1State::next(bool input){
    bool temp = getI(16);
    head = head<<1;
    tail = tail<<1;
    setI(47,input);
    setI(15, temp);
}

void Crypto1State::prev(bool input){
    bool temp  =getI(32);
    head = head>>1;
    tail = tail>>1;
    setI(47, input);
    setI(31,temp);
}

void Crypto1State::printAsString(){
    bitset<16> headset(head);
    bitset<32> tailset(tail);
    cout<<headset<<tailset<<"\n";
}
