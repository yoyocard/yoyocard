#include "Crypto1.h"
#include "Crypto1State.h"
#include <math.h>
#include <iostream>
#include <cstdlib>

using namespace std;
//uint32_t NumberOfSetBits(uint32_t i);
uint32_t reverses(uint32_t x);

Crypto1::Crypto1(Crypto1State state, int t=0):internalState(state), fa(0x26c7), fb(0x0dd3), fc(0x4457c3b8)
{
    time = t;
    tempCount = 0;
}

void Crypto1::setInternalState(Crypto1State state){
    internalState = state;
}

bool Crypto1::nextState(bool input){
    bool temp = internalState.getI(0);
    internalState.next(internalState.getI(0)!=internalState.getI(5)!=internalState.getI(9)
    !=internalState.getI(10)!=internalState.getI(12)!=internalState.getI(14)
    !=internalState.getI(15)!=internalState.getI(17)!=internalState.getI(19)
    !=internalState.getI(24)!=internalState.getI(25)!=internalState.getI(27)
    !=internalState.getI(29)!=internalState.getI(35)!=internalState.getI(39)
    !=internalState.getI(41)!=internalState.getI(42)!=internalState.getI(43)
    !=input);
    return temp;
}

bool Crypto1::prevState(bool input){
    return false;
}

Crypto1State Crypto1::getInternalState(){
    return internalState;
}

bool Crypto1::getKeyStream(){
    int totalSum = 0;
    for(int k=0;k<5;k++){
        int partialSum = 0;
        if(internalState.getI(41-k*8))partialSum+=8;
        if(internalState.getI(43-k*8))partialSum+=4;
        if(internalState.getI(45-k*8))partialSum+=2;
        if(internalState.getI(47-k*8))partialSum+=1;
        if(k==1||k==4){if(fa[partialSum])totalSum+=pow(2,k);}
        else {if(fb[partialSum])totalSum+=pow(2,k);}
    }
    //cout<<fc[totalSum]<<" "<<tempCount++<<endl;
    return fc[totalSum];
}

 int Crypto1::sumProperty(){
    Crypto1State tempState(internalState);
    int sp=0;//sumProperty
    //int gg[256][2]={0};
    for(int i = 0 ; i < 256; i++){
        bitset<8> bs(i);
        //int temp2=0;
        bool temp = false;
        for(int j=0;j<9;j++){
            //temp2 = temp2<<1;
            temp = (temp!=getKeyStream());
            if(j!=8){
                //temp2 |= (getKeyStream()!=bs[j]);
                nextState(bs[j]);
            }else{
                //temp2 |= (getKeyStream()!=(NumberOfSetBits(i)%2));
            }
        }
        //gg[temp2>>1][0] = temp2;
        //gg[temp2>>1][1] = i;
        if(!temp) sp++;
        setInternalState(tempState);
    }
    /*for(int i=0;i<256;i++){
        cout<<bitset<9>(gg[i][0])<<" "<<bitset<8>(gg[i][1])<<endl;
    }*/
    return sp;
}

void Crypto1::eoSumProperty(uint8_t *sp){
    Crypto1State tempState(internalState);
    uint8_t oSp=0;
    uint8_t eSp=0;
    for(int i =0; i < 16; i++){
        bitset<4> bs(i);
        bool odd = false;
        bool even = false;
        for(int j=0;j<9;j++){
            if((j%2)==0) {
                odd = (odd!=getKeyStream());
                if(j!=8){
                    nextState(bs[j/2]);
                }
            }
            else{
                even = (even!=getKeyStream());
                nextState(bs[j/2]);
            }
        }
        if(odd) oSp++;
        if(even) eSp++;
        setInternalState(tempState);
    }
    sp[0] = oSp;
    sp[1] = eSp;
    //cout<<"odd:"<<(unsigned)oSp<<" even:"<<(unsigned)eSp<<endl;

}

/**
* This function generate a 36 bit nonce? given 32 bit input.
* The output is not xored with the nonce as it is in real crypto1.
* This is because this function is only for testing the algorithm,
* the calculation of sum property would cancel out the effect of nonce,
* there's no need to do it twice.
*/

uint64_t Crypto1::generateANonce(uint32_t input){
    uint64_t result = 0;
    int gg = input>>24;
    bitset<32> ks;
    //cout<<endl<<bitset<32>(input)<<endl;
    input = reverses(input); //reverse the input to make it easy to do
    uint8_t temp = 0b0;
    for(int i=0;i<32;i++){
        result = result<<1;
        if(getKeyStream()!=((bool)(input&0b1))) result |= 0b1;
        ks[31-i] = getKeyStream();
        temp = temp^(input&0b1);
        nextState((input&0b1));
        input = input>>1;
        if((i%8)==7){
            result = result<<1;
            if(getKeyStream()==(bool)temp) result |= 0b1;
            //if(i==7) cout<<(unsigned)temp<<endl;
            temp=0b0;
        }
    }
    //cout<<ks<<endl;
    //cout<<bitset<36>(result)<<" "<<bitset<8>(gg)<<endl;
    //system("pause");
    return result;
}

uint32_t reverses(uint32_t x)
{
    x = ((x >> 1) & 0x55555555u) | ((x & 0x55555555u) << 1);
    x = ((x >> 2) & 0x33333333u) | ((x & 0x33333333u) << 2);
    x = ((x >> 4) & 0x0f0f0f0fu) | ((x & 0x0f0f0f0fu) << 4);
    x = ((x >> 8) & 0x00ff00ffu) | ((x & 0x00ff00ffu) << 8);
    x = ((x >> 16) & 0xffffu) | ((x & 0xffffu) << 16);
    return x;
}
/*uint32_t NumberOfSetBits(uint32_t i)
{
     i = i - ((i >> 1) & 0x55555555);
     i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
     return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}*/

