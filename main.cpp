#include <iostream>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <set>
#include "Crypto1.h"
#include "Crypto1State.h"
#include <random>
#include <algorithm>
#include <stdexcept>
#define CHECK_BIT(var,pos) ((var) & (1<<(pos)))

/**should use c++ 11*/

using namespace std;

Crypto1State getStateFrom2Int(int,int);
double binomial_coefficient(uint64_t n, uint64_t k);
double HyperGeometric(int N, int K, int k, int n);
uint32_t NumberOfSetBits(uint32_t i);

int main()
{
    /**
    * TODO: determine whether to use an array of size 2^20 to have
    * quick access to specific interval. Since we'll have to match 4-8 bits of
    * first sum property to 0-3 bits of second sum property.
    * would it cost a lot to iterate through all possibilities?
    */

    /** Used to calculate the time used in each phase */
    time_t tstart, tend;

    /** Table that contains the even and odd sum property of a specific state */
    uint8_t *oTable = new uint8_t[1024*1024];
    uint8_t *eTable = new uint8_t[1024*512];

    /** Vectors that stores the state with specific sum property*/
    vector<int> oVector[17];
    vector<int> eVector[17];

    /** Vector that contains nonce of different 8 start bits*/
    vector<uint64_t> nVector[256];

    /** Pseudo random number generator, usage rng()*/
    mt19937 rng(time(0));

    /** head and tail for the Crypto1State */
    uint16_t head = (uint16_t) rng();
    uint32_t tail = rng();
    Crypto1 mCrypto1(Crypto1State(head, tail), 0);

    cout<<"the first sumproperty of the initial state is: "<<mCrypto1.sumProperty()<<endl;

    //goto skip;
    /** Calculating sum property for each state and sort them into vectors */
    uint8_t temp[2];
    mCrypto1.eoSumProperty(temp);
    //cout<<"odd:"<<temp[0]<<" even:"<<temp[1]<<endl;
    //system("pause");
    cout<<"phase 1 construct even odd sum property table"<<endl;
    tstart = time(0);

    /** calculate sum property of even and odd at the same time for 0-1024*512 for convenience */
    for(int i=0;i<1024*512;i++){
        //the function would fill the input array pointer with [odd sp, even sp]
        Crypto1(getStateFrom2Int(i,i),0).eoSumProperty(temp);
        *(oTable+i) = temp[0];
        oVector[temp[0]].push_back(i);
        *(eTable+i) = temp[1];
        eVector[temp[1]].push_back(i);
    }
    for(int i=1024*512;i<1024*1024;i++){
        Crypto1(getStateFrom2Int(i,0),0).eoSumProperty(temp);
        *(oTable+i) = temp[0];
        oVector[temp[0]].push_back(i);
    }

    tend = time(0);
    cout << "It took "<< difftime(tend, tstart) <<" second(s)."<< endl;
    //skip:
    /** Start collecting nonce*/
    cout<<"phase 2 collecting nonce"<<endl;
    tstart = time(0);
    /** numberCount: count if all 8 bit starts have been collected
    *   second: the value of first 8 bits to calculate the second SP
    */
    int numberCount = 0, second, collected = 0;
    while(1){
        collected++;
        mCrypto1.setInternalState(Crypto1State(head, tail));
        uint64_t temp = mCrypto1.generateANonce(rng());
        if(nVector[(temp>>28)].size() == 0) {
                numberCount++;
                nVector[(temp>>28)].push_back(temp);
        }
        if(nVector[(temp>>28)].size()!=0)
            for(int i=0;i<nVector[(temp>>28)].size();i++){
                if(nVector[(temp>>28)][i]>>19==temp>>19) break;
                else if(i == nVector[(temp>>28)].size()-1) nVector[(temp>>28)].push_back(temp);
            }
        /** Stop collecting nonce if all 256 possibility is collected and
        *   there's 20 nonce of any starts.
        *   *** There may be duplicates, with little possibilities, ignore it for convenience
        */
        if(nVector[(temp>>28)].size()>=50 && numberCount == 256){
            second = temp>>28;
            break;
        }
    }
    cout<<"total collect "<<collected<<"nonce"<<endl;

    /** calculating sum property of collected bits */
    int firstSumProperty = 0;
    for(int i=0; i<256; i++){
        //cout<<bitset<36>(nVector[i][0])<<endl;
        //cout<<bitset<9>(nVector[i][0]>>27)<<endl;
        if(NumberOfSetBits(nVector[i][0]>>27)%2){
            firstSumProperty++;
        }
    }

    cout << "first sumproperty calculated is "<<firstSumProperty<<endl;
    /** find distribution of sum property */
    double sumPropertyDistribution[257] = {0};
    for(int i=0;i<10000;i++){
        uint16_t h = (uint16_t) rng();
        uint32_t t = (uint32_t) rng();
        sumPropertyDistribution[Crypto1(Crypto1State(h, t), 0).sumProperty()]++;
    }
//    for(int i=0;i<257;i++){
//        cout<<i<<":"<<sumPropertyDistribution[i]<<endl;
//    }
    /** find the most possible second sum property */
    int secondSumProperty=-1;
    double now = -1;
    int smallK = 0;
    for(int i=0;i<nVector[second].size();i++){
            if(NumberOfSetBits((nVector[second][i]>>18)&0b000000000111111111)%2){
                smallK++;
            }
        }
    for(int i=0;i<257;i++){
        double divider = 0;
        if(sumPropertyDistribution[i] == 0) continue;
        for(int j=0;j<257;j++){
            divider+=(((double) HyperGeometric(256, j, nVector[second].size(), smallK))*
                      (sumPropertyDistribution[j]/(double) 10000));
        }
        double prob = ((double) HyperGeometric(256, i, nVector[second].size(), smallK))*
        ( sumPropertyDistribution[i]/(double) 10000)/divider;
        cout<<"i="<<i<<" "<<"prob="<<prob<<endl;
        if(prob>now){
            secondSumProperty = i;
            now = prob;
        }
    }

    tend = time(0);
    cout << "It took "<< difftime(tend, tstart) <<" second(s)."<< endl;
    cout << "first sumproperty calculated is "<<firstSumProperty<<endl;

    cout<<"phase 3 calculating"<<endl;
    tstart = time(0);
    vector<int> p,q,r,s;
    set<int> ps,qs,rs,ss;
    for(int i=0;i<17;i++){
        for(int j=0;j<17;j++){
            if((i*(16-j)+j*(16-i))==firstSumProperty){
                p.push_back(i);//first odd
                ps.insert(i);
                q.push_back(j);//first even
                qs.insert(j);
                cout<<"p="<<i<<" q="<<j<<endl;
            }
            if((i*(16-j)+j*(16-i))==128){
                r.push_back(i);
                s.push_back(j);
                rs.insert(i);//second odd
                ss.insert(j);//second even
            }
        }
    }

    int total_possible_odd=0, total_possible_even=0;
    cout<<"size of ps is "<<ps.size()<<endl;
    for(auto i : ps){
            cout<<i<<" "<<oVector[i].size()<<endl;
        for(int j=0;j<oVector[i].size();j++){
            for(int k=0;k<16;k++){
                if(rs.find(oTable[((oVector[i][j]<<4)&0b11111111111111111111)+k])!=rs.end()){
                    total_possible_odd++;
                }
            }
        }
    }
    cout<<"size of qs is "<<qs.size()<<endl;
    for(auto i : qs){
            cout<<i<<" "<<eVector[i].size()<<endl;
            cout<<"total possible even"<<total_possible_even<<endl
            system("pause");
        for(int j=0;j<eVector[i].size();j++){
            for(int k=0;k<16;k++){
                if(ss.find(eTable[((eVector[i][j]<<4)&0b1111111111111111111)+k])!=ss.end()){
                    total_possible_even++;
                }
            }
        }
    }
    int totalCountOdd = 0;
    int totalCountEven= 0;
    for(int i=0;i<17;i++){
        totalCountOdd+=oVector[i].size();
        totalCountEven+=eVector[i].size();
    }
    cout<<"total odd:"<<totalCountOdd<<endl<<"total even:"<<totalCountEven<<endl;

    cout<<"total possivle odds="<<total_possible_odd<<endl;
    cout<<"total possivle evens="<<total_possible_even<<endl;
    tend = time(0);
    cout << "It took "<< difftime(tend, tstart) <<" second(s)."<< endl;
    free(oTable);
    free(eTable);
    return 0;
}

Crypto1State getStateFrom2Int(int odd, int even){
    uint16_t head = 0;
    uint32_t tail = 0;
    for(int i=0;i<16;i++){
        tail |= ((odd&(0b1<<i))<<i);
        tail |= ((even&(0b1<<i))<<(i+1));
    }
    for(int i=16;i<21;i++){
        head |= ((odd&(0b1<<i))>>(32-i));
        head |= ((even&(0b1<<i))<<(31-i));
    }
    return Crypto1State(head, tail);
}

/** Copied from StackOverflow, should return the bits set in an integer*/
uint32_t NumberOfSetBits(uint32_t i)
{
     i = i - ((i >> 1) & 0x55555555);
     i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
     return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}

double HyperGeometric(int N, int K, int n, int k){
    return (binomial_coefficient(K,k)*binomial_coefficient(N-K, n-k)/binomial_coefficient(N,n));
}

double binomial_coefficient(uint64_t n, uint64_t k)
{
    if (k > n){
        return 0;
    }
    if (n - k < k)
        k = n - k;

    double result = 1;

    for (uint64_t d = 0; d < k; d++)
    {
        result *= (n-d);
        result /= (k-d);
    }

    return result;
}
